#!/usr/bin/env bash
#COPY ANDROID SDK LICENSE
# cp -vr ./scripts/licenses "$ANDROID_HOME/"

docker run \
	-it --rm \
	-v "$PWD":/application \
	-v "$HOME/.gradle":/Users/sanju/.gradle \
    -v "$ANDROID_HOME":/Users/sanju/Library/Android/sdk \
	volumeandroiddocker.image \
    sh -c "$@"
