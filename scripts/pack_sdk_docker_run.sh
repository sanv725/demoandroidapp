#!/usr/bin/env bash
set -xeuo pipefail

docker run \
	--rm \
	-v "$PWD":/application \
	packsdkandroiddocker.image \
	sh -c 'for file in ./*; do echo ${file}; done;'